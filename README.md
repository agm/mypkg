# mypkg

This package mypkg is a template package for the development 
of plugins for Axl.

## Building the source

To configure, you can use cmake as follows:

> mkdir build; 
> cd build;
> cmake ../mypkg -DAxl_DIR=@folder_where_you_build_Axl@

This will build a library mypkg and put it in the folder build/lib

* If you don't want to build the Axl plugin, you can type:

> cmake ../mypkg -DAXL=OFF

* If you want to generate the documentation, you can type:

> cmake ../mypkg -DDOC=ON

These options can be combined or changed directly using 

> ccmake ../mypkg 
