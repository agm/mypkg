####################################################################
## Build apps
####################################################################
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

file (GLOB cppFiles RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"  "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

foreach (file ${cppFiles})
    string (REGEX REPLACE ".cpp\$" "" outfileName "${file}")
    add_executable(${outfileName} ${CMAKE_CURRENT_SOURCE_DIR}/${file} )
    qt5_use_modules (${outfileName} Xml)
endforeach (file)

