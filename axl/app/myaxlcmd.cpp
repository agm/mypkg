#include <axl-config.h>
#include <axlCore/axlAbstractData.h>
#include <axlCore/axlReader.h>
#include <axlCore/axlWriter.h>
#include <axlCore/axlFactoryRegister.h>
#include <dtkCoreSupport/dtkPluginManager.h>
#include <axlCore/axlAbstractCurveBSpline.h>

int main(int argc, char** argv) { 

  if (argc <2) { 
    std::cout<< "usage: "<<argv[0]<< " file.axl"<<std::endl; 
    return 1;
  }
    // Loading of Axl plugins
    dtkPluginManager::instance()->setPath(AXL_PLUGIN_DIR);
    dtkPluginManager::instance()->initializeApplication();
    dtkPluginManager::instance()->initialize();
    axlFactoryRegister::initialized();

    //Reading input file
    qDebug()<<"Reading file input.axl";
    axlReader *obj_reader = new axlReader();
    obj_reader->read(QString(argv[1]));
    QList<axlAbstractData *> list = obj_reader->dataSet();

    // Print description of data0.
    qDebug()<<list.at(0)->description();

    /* <code> */

    double x=0.9, y=1.5, z=0;
    axlPoint* X= new axlPoint(x,y,z);
    X->setColor(1.0,0,0);
    X->setSize(0.02);

    /* </code> */

    //Writing output file
    axlWriter *obj_writer = new axlWriter();
    // All the data of the file input.axl
    foreach(axlAbstractData* o, list)
        obj_writer->addDataToWrite(o);
    // and the point X.
    obj_writer->addDataToWrite(X);

    obj_writer->write("output.axl");
    qDebug()<<"Output: axel output.axl";

    //system ("axel output.axl &");
}
