///////////////////////////////////////////////////////////////////
// Generated by axl-plugin wizard
///////////////////////////////////////////////////////////////////
/* (C) MyCompany */
/* Put a short description of your plugin here */
/* man-c@mycompany.com http://www.mycompany.com */

#include "axlAnimationLineExample.h"
#include "myAnimationPlugin.h"

#include <axlCore/axlAbstractData.h>
#include <axlCore/axlLine.h>
#include <QColor>

#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>

//Include from my package
//#include <mypkg/my_class.h>

// /////////////////////////////////////////////////////////////////
// axlAnimationLineExamplePrivate
// /////////////////////////////////////////////////////////////////

class axlAnimationLineExamplePrivate
{
public:
    QPair<dtkAbstractData *, dtkAbstractData *> input;

    axlAbstractData *output;
};

// /////////////////////////////////////////////////////////////////
// axlAnimationLineExample
// /////////////////////////////////////////////////////////////////

axlAnimationLineExample::axlAnimationLineExample(void) : axlAbstractProcess(), d(new axlAnimationLineExamplePrivate)
{
    d->output = NULL;
}

axlAnimationLineExample::~axlAnimationLineExample(void)
{
    delete d;
    d = NULL;
}

void axlAnimationLineExample::setInput(dtkAbstractData *data, int channel)
{
    if(channel==0)
        d->input.first = data;
    else if(channel==1)
        d->input.second = data;
    else
        qDebug()<<"Only two channel available : 0 or 1";
}

dtkAbstractData *axlAnimationLineExample::output(void)
{
    return d->output;
}

// interpolation for axlPoints (needed for QPropertyAnimation)
QVariant axlAnimationLineExample::axlPointInterpolator(const axlPoint &start, const axlPoint &end, qreal progress) 
{
    double theta = progress*6.3;
    double x = cos(theta) * start.x() + sin(theta) * start.y();
    double y = -sin(theta) * start.x() + cos(theta) * start.y();
    double z = 0;

    return QVariant::fromValue(axlPoint(x,y,z));
}

int axlAnimationLineExample::update(void)
{
    // register the animation interpolator for axlPoint objects;
    // because we want to animate a point (axlPoint) of the line
    // and axlPoint isn't a supported variant for QPropertyAnimation,
    // its interpolation needs to be implemented (axlPointInterpolator)
    qRegisterAnimationInterpolator<axlPoint>(axlPointInterpolator);

    // create an axlLine to animate
    axlLine* L = new axlLine();

    // create the QPropertyAnimation object
    // QPropertyAnimation class animates Qt properties
    // here the qproperty 'point2' of an axlLine is animated
    QPropertyAnimation* animation = new QPropertyAnimation(L, "point2");
    // set the properties of the animation (duration, start and end value)
    animation->setDuration(4000);
    animation->setStartValue(QVariant::fromValue(axlPoint(0,1,0)));
    animation->setEndValue(QVariant::fromValue(axlPoint(0,1,0)));

    // Set the output of the process;
    d->output=L;

    // start the animation
    animation->start();

    return 1;
}


QString  axlAnimationLineExample::form(void) const
{
    return QString(
                " OUTPUT:0 data axlLine \n");
}

bool axlAnimationLineExample::registered(void)
{
    return myAnimationPlugin::processFactSingleton->registerProcessType("axlAnimationLineExample", //The identifier of the process
                                                              create_myAnimation_axlAnimationLineExample, // The constructor function
                                                              "Process" // The tag which appears in the tool cloud.
                                                              );
}

QString axlAnimationLineExample::description(void) const
{
    return "MyProc description";
}

QString axlAnimationLineExample::identifier(void) const
{
    return "axlAnimationLineExample";
}

////////////////////////////////////////////////////////////////////
// Type instanciation
////////////////////////////////////////////////////////////////////

dtkAbstractProcess *create_myAnimation_axlAnimationLineExample(void)
{
    return new axlAnimationLineExample;
}

