/***********************************************************************
 * myAnimationPlugin.cpp
 *
 * Description: source file for axel plugin
 * 
 * Generated with: mmk 
 * Author: man-c
 *  
 *   
 * Date: Wed Mar  7 18:09:25 2018
 * Changelog: 
 *
 ***********************************************************************/
#include "myAnimationPlugin.h" 
/* <header> */
#include "axlAnimationLineExample.h"
#include "axlAnimationSphereExample.h"
#include "axlAnimationMultipleSpheresExample.h"
/* </header> */

#include <axlGui/axlInspectorObjectFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkLog/dtkLog.h>

// /////////////////////////////////////////////////////////////////
// myAnimationPrivate
// /////////////////////////////////////////////////////////////////

class myAnimationPluginPrivate
{
public:
    // Class variables go here.
};

// /////////////////////////////////////////////////////////////////
// myAnimation
// /////////////////////////////////////////////////////////////////

myAnimationPlugin::myAnimationPlugin(QObject *parent) : dtkPlugin(parent), d(new myAnimationPluginPrivate)
{
    
    
}

myAnimationPlugin::~myAnimationPlugin(void)
{
    delete d;
    
    d = NULL;
}

/*!
 * \brief Registers the data, processes and interfaces in their factories.
 */
bool myAnimationPlugin::initialize(void)
{
    dtkWarn()<<"initialize myAnimationPlugin";
    myAnimationPlugin::dataFactSingleton = dtkAbstractDataFactory::instance();
    //dataFactorySingleton();
    myAnimationPlugin::processFactSingleton = dtkAbstractProcessFactory::instance();
    //processFactorySingleton();

    /* <registration> */
    if(!axlAnimationLineExample::registered()) { dtkWarn() << "Unable to register axlAnimationLineExample type"; }
    if(!axlAnimationSphereExample::registered()) { dtkWarn() << "Unable to register axlAnimationSphereExample type"; }
    if(!axlAnimationMultipleSpheresExample::registered()) { dtkWarn() << "Unable to register axlAnimationMultipleSpheresExample type"; }
    /* </registration> */

    return true;
}

bool myAnimationPlugin::uninitialize(void)
{
    return true;
}

/*!
 * \brief name of the plugin
 * \return a string, which is the name of the plugin
 */
QString myAnimationPlugin::name(void) const
{
    return "myAnimationPlugin";
}
/*!
 * \brief The description of the plugin.
 */
QString myAnimationPlugin::description(void) const
{
    return "a description of my plugin";
}

QStringList myAnimationPlugin::tags(void) const
{
  return QStringList()
    << "Process";
}

/*!
 * \brief List of type names of data, processes, interfaces that the plugin provides.
 * \return a list of strings containing the types handled by the plugin.
 */
QStringList myAnimationPlugin::types(void) const
{
    return QStringList()
    /* <types> */
        << "axlAnimationLineExample"
        << "axlAnimationSereExample"
        << "axlAnimationMultipleSeresExample"
    /* </types> */
    ;

}

dtkAbstractDataFactory *myAnimationPlugin::dataFactSingleton = NULL;
dtkAbstractProcessFactory *myAnimationPlugin::processFactSingleton = NULL;


