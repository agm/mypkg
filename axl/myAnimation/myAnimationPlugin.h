/***********************************************************************
 * myAnimationPlugin.h
 *
 * Description: source file for axel plugin
 * 
 * Generated with: mmk 
 * Author: man-c
 *  
 *   
 * Date: Wed Mar  7 18:09:25 2018
 * Changelog: 
 *
 ***********************************************************************/

#pragma once

#include <dtkCoreSupport/dtkPlugin.h>

#include "myAnimationExport.h"

class dtkAbstractDataFactory;
class dtkAbstractProcessFactory;

class myAnimationPluginPrivate;

class MYANIMATIONPLUGIN_EXPORT myAnimationPlugin : public dtkPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.myAnimationPlugin" FILE "myAnimationPlugin.json")

public: 
    myAnimationPlugin(QObject *parent = 0);
    ~myAnimationPlugin(void);
    
    virtual bool initialize(void);
    virtual bool uninitialize(void);
    
    virtual QString name(void) const;
    virtual QString description(void) const;
    
    virtual QStringList tags(void) const;
    virtual QStringList types(void) const;
    
public:
    static dtkAbstractDataFactory *dataFactSingleton;
    static dtkAbstractProcessFactory *processFactSingleton;
    
    
private:
    myAnimationPluginPrivate *d;
};

