// /////////////////////////////////////////////////////////////////
// Generated by axl-plugin wizard
// /////////////////////////////////////////////////////////////////
/* (C) MyCompany */
/* Put a short description of your plugin here */
/* my-contact@mycompany.com   http://www.mycompany.com */

#pragma once

#include "myToolsExport.h"

#include "myToolsPlugin.h"

#include <axlCore/axlAbstractData.h>

class myDataPrivate;

class MYTOOLSPLUGIN_EXPORT myData :  public axlAbstractData
{
    Q_OBJECT
    
public:
    /// Creates an uninitialized SplineSurface, which can only be assigned to
    /// or read(...) into.
    myData(void);
    
    /// Virtual destructor, enables safe inheritance.
    virtual ~myData(void);
    
    virtual QString description(void) const;
    virtual QString identifier(void) const;
    virtual QString objectType(void) const;
    
    static bool registered(void);
    
private:
    myDataPrivate *d;
};

dtkAbstractData *create_myTools_myData(void);


