// /////////////////////////////////////////////////////////////////
// Generated by axl-plugin wizard
// /////////////////////////////////////////////////////////////////

/* (C) MyCompany */


/* Put a short description of your plugin here */

/* MyCompany-contact@mycompany.com-http://www.mycompany.com */

#ifndef MYTOOLSDEFAULTDATACREATORPROCESSDIALOG_H
#define MYTOOLSDEFAULTDATACREATORPROCESSDIALOG_H

# include <axlCore/axlAbstractData.h>
# include <axlGui/axlInspectorToolFactory.h>
#include "myToolsExport.h"

class myDataCreatorDialogPrivate;

class MYTOOLSPLUGIN_EXPORT myDataCreatorDialog : public axlInspectorToolInterface {
    Q_OBJECT
    
public:
    myDataCreatorDialog(QWidget* parent = 0);
    virtual ~myDataCreatorDialog();
    
    static bool registered(void);
    
signals:
    void dataInserted(axlAbstractData* data);
    
public slots:
    void run(void);
    
private:
    myDataCreatorDialogPrivate* d;
    
};

dtkAbstractProcess* create_myTools_ProcessCreatormyData(void);
axlInspectorToolInterface* create_myTools_myDataCreatorDialog(void);

#endif // MYTOOLSDEFAULTDATACREATORPROCESSDIALOG_H

