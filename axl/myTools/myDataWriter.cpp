// /////////////////////////////////////////////////////////////////
// Generated by axl-plugin wizard
// /////////////////////////////////////////////////////////////////

/* (C) MyCompany */
/* Put a short description of your plugin here */
/* MyCompany-contact@mycompany.com-http://www.mycompany.com */

#include "myDataWriter.h"

#include "myData.h"
#include "myToolsPlugin.h"

#include <dtkCoreSupport/dtkAbstractData.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>

// /////////////////////////////////////////////////////////////////
// myDataWriter
// /////////////////////////////////////////////////////////////////

myDataWriter::myDataWriter(void)
{
    
}

myDataWriter::~myDataWriter(void)
{
    
}

QString myDataWriter::identifier(void) const
{
    return "myDataWriter";
}

QString myDataWriter::description(void) const
{
    return "myDataWriter";
}

QStringList myDataWriter::handled(void) const
{
    return QStringList() << "myData";
}

bool myDataWriter::registered(void)
{
    return myToolsPlugin::dataFactSingleton->registerDataWriterType("myDataWriter", QStringList(),
                                                              create_myTools_myDataWriter);
}

bool myDataWriter::accept(dtkAbstractData *data)
{
    myData *pluginData = dynamic_cast<myData *>(data);
    if(pluginData)
        return true;
    
    return false;
}

bool myDataWriter::reject(dtkAbstractData *data)
{
    return !this->accept(data);
}

QDomElement myDataWriter::write(QDomDocument *doc, dtkAbstractData *data)
{
    //TODO : Replace myData by your new data type and write into the QDomElement its description
    myData *mydata = dynamic_cast<myData *>(data);
    
    QDomElement dataElement =  doc->createElement("myDataTag");
    
    return dataElement;
}


dtkAbstractDataWriter *create_myTools_myDataWriter(void)
{
    return new myDataWriter;
}

