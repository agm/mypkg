/***********************************************************************
 * myProcess.h
 *
 * Description: header file for axel process.
 * 
 * Generated with: mmk 
 * Author: my-contact
 * (C) my-company 
 * my-email my-website 
 * Date: my-date
 * Changelog: 
 *
 ***********************************************************************/
#pragma once

#include "myToolsExport.h"

#include <axlCore/axlAbstractProcess.h>

class axlAbstractData;

class myProcessPrivate;

class MYTOOLSPLUGIN_EXPORT myProcess: public axlAbstractProcess
{
    Q_OBJECT
    
public:
    myProcess(void);
    virtual ~myProcess(void);
    
    virtual QString description(void) const;
    virtual QString identifier(void) const;
    
public:
    static bool registered(void);
    
public:
    dtkAbstractData *output(void);
    
public slots:
    void setInput(dtkAbstractData *data, int channel);
    
public slots:
    int update(void);
    
private:
    myProcessPrivate *d;
};

dtkAbstractProcess *create_myTools_myProcess(void);


