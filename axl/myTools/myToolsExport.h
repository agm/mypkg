// /////////////////////////////////////////////////////////////////
// Generated by axl-plugin wizard
// /////////////////////////////////////////////////////////////////
/* (C) MyCompany */
/* Put a short description of your plugin here */
/* MyCompany-contact@mycompany.com-http://www.mycompany.com */

#ifndef MYTOOLSPLUGINEXPORT_H
#define MYTOOLSPLUGINEXPORT_H

#ifdef WIN32
   #ifdef MYTOOLSPLUGIN_EXPORT
      #define MYTOOLSPLUGIN_EXPORT __declspec(dllexport)
   #else
      #define MYTOOLSPLUGIN_EXPORT
   #endif
#else
   #define MYTOOLSPLUGIN_EXPORT
#endif

#endif

