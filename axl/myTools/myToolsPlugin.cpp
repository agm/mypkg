/***********************************************************************
 * myToolsPlugin.cpp
 *
 * Description: source file for axel plugin
 * 
 * Generated with: mmk 
 * Author: my-contact
 * (C) my-company 
 * my-email my-website 
 * Date: my-date
 * Changelog: 
 *
 ***********************************************************************/
#include "myToolsPlugin.h" 
/* <header> */
#include "myProcess.h"
#include "myProcessDialog.h"
#include "myProcessWithForm.h"
#include "myData.h"
#include "myDataDialog.h"
#include "myDataReader.h"
#include "myDataWriter.h"
#include "myDataConverter.h"
#include "myDataCreatorDialog.h"
/* </header> */

#include <axlGui/axlInspectorObjectFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkLog/dtkLog.h>

// /////////////////////////////////////////////////////////////////
// myToolsPrivate
// /////////////////////////////////////////////////////////////////

class myToolsPluginPrivate
{
public:
    // Class variables go here.
};

// /////////////////////////////////////////////////////////////////
// myTools
// /////////////////////////////////////////////////////////////////

myToolsPlugin::myToolsPlugin(QObject *parent) : dtkPlugin(parent), d(new myToolsPluginPrivate)
{
    
    
}

myToolsPlugin::~myToolsPlugin(void)
{
    delete d;
    
    d = NULL;
}

/*!
 * \brief Registers the data, processes and interfaces in their factories.
 */
bool myToolsPlugin::initialize(void)
{
    dtkWarn()<<"initialize myToolsPlugin";
    myToolsPlugin::dataFactSingleton = dtkAbstractDataFactory::instance();
    //dataFactorySingleton();
    myToolsPlugin::processFactSingleton = dtkAbstractProcessFactory::instance();
    //processFactorySingleton();

    /* <registration> */
    if(!myData::registered()) { dtkWarn() << "Unable to register myData type";}
    if(!myDataDialog::registered()) { dtkWarn() << "Unable to register myDataDialog type"; }
    if(!myDataReader::registered()) { dtkWarn() << "Unable to register myDataReader type"; }
    if(!myDataWriter::registered()) { dtkWarn() << "Unable to register myDataWriter type"; }
    if(!myDataConverter::registered()) { dtkWarn() << "Unable to register myDataConverter type"; }
    if(!myDataCreatorDialog::registered()) { dtkWarn() << "Unable to register myDataCreatorDialog type"; }
    if(!myProcess::registered()) { dtkWarn() << "Unable to register myProcess type"; }
    if(!myProcessDialog::registered()) { dtkWarn() << "Unable to register myProcessDialog type"; }
    if(!myProcessWithForm::registered()) { dtkWarn() << "Unable to register myProcess type"; }
    /* </registration> */

    return true;
}

bool myToolsPlugin::uninitialize(void)
{
    return true;
}

/*!
 * \brief name of the plugin
 * \return a string, which is the name of the plugin
 */
QString myToolsPlugin::name(void) const
{
    return "myToolsPlugin";
}
/*!
 * \brief The description of the plugin.
 */
QString myToolsPlugin::description(void) const
{
    return "a description of my plugin";
}

QStringList myToolsPlugin::tags(void) const
{
  return QStringList()
    << "Process";
}

/*!
 * \brief List of type names of data, processes, interfaces that the plugin provides.
 * \return a list of strings containing the types handled by the plugin.
 */
QStringList myToolsPlugin::types(void) const
{
    return QStringList()
    /* <types> */
      << "myProcess"
      << "myProcessDialog"
      << "myData"
      << "myDataDialog"
      << "myDataReader"
      << "myDataWriter"
      << "myDataConverter"
      << "myDataCreatorDialog"
    /* </types> */
    ;

}

dtkAbstractDataFactory *myToolsPlugin::dataFactSingleton = NULL;
dtkAbstractProcessFactory *myToolsPlugin::processFactSingleton = NULL;


