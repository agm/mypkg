/***********************************************************************
 * myToolsPlugin.h
 *
 * Description: source file for axel plugin
 * 
 * Generated with: mmk 
 * Author: my-contact
 * (C) my-company 
 * my-email my-website 
 * Date: my-date
 * Changelog: 
 *
 ***********************************************************************/

#pragma once

#include <dtkCoreSupport/dtkPlugin.h>

#include "myToolsExport.h"

class dtkAbstractDataFactory;
class dtkAbstractProcessFactory;

class myToolsPluginPrivate;

class MYTOOLSPLUGIN_EXPORT myToolsPlugin : public dtkPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.myToolsPlugin" FILE "myToolsPlugin.json")

public: 
    myToolsPlugin(QObject *parent = 0);
    ~myToolsPlugin(void);
    
    virtual bool initialize(void);
    virtual bool uninitialize(void);
    
    virtual QString name(void) const;
    virtual QString description(void) const;
    
    virtual QStringList tags(void) const;
    virtual QStringList types(void) const;
    
public:
    static dtkAbstractDataFactory *dataFactSingleton;
    static dtkAbstractProcessFactory *processFactSingleton;
    
    
private:
    myToolsPluginPrivate *d;
};

