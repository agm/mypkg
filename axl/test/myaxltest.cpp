#include <axl-config.h>
#include <mypkg-config.h>
#include <axlCore/axlAbstractData.h>
#include <axlCore/axlReader.h>
#include <axlCore/axlWriter.h>
#include <axlCore/axlFactoryRegister.h>
#include <dtkCoreSupport/dtkPluginManager.h>
#include <axlCore/axlAbstractCurveBSpline.h>

int main() {

    // Loading of Axl plugins
    dtkPluginManager::instance()->setPath(AXL_PLUGIN_DIR);
    dtkPluginManager::instance()->initializeApplication();
    dtkPluginManager::instance()->initialize();
    axlFactoryRegister::initialized();

    //Reading input file
    qDebug()<<"Reading file"<<QString(MYPKG_DATA_DIR)+"/input.axl";
    axlReader *obj_reader = new axlReader();
    obj_reader->read(QString(MYPKG_DATA_DIR)+"/input.axl");
    QList<axlAbstractData *> list = obj_reader->dataSet();

    // Print description of data0.
    if(list.size() >0) qDebug()<<list.at(0)->description();

    /* <code> */
    double x=0.9, y=1.5, z=0;
    axlPoint* X= new axlPoint(x,y,z);
    X->setColor(1.0,0,0);
    X->setSize(0.02);
    /* </code> */

    //Writing output file
    axlWriter *obj_writer = new axlWriter();
    foreach(axlAbstractData* o, list)
        obj_writer->addDataToWrite(o);
    obj_writer->addDataToWrite(X);
    obj_writer->write("output.axl");
    qDebug()<<"Output: axel output.axl";

}
