======================
Creating an Axl plugin
======================

This tutorial describes how to construct a package with a plugin for axl.
We assume that ``axl`` has been built in the directory ``build-axl``.

Installation
============

The first step is to get the package ``mypkg``, which provides a template for the
package and plugin structure:
::
   
   git clone https://gitlab.inria.fr/axl/mypkg.git mypkg


The package provides the script ``mmk`` is written with ``perl``, which should be available in your environment. The ``perl`` module ``File/Find/Rule`` should also be installed::

   sudo perl -MCPAN -e'install File::Find::Rule'
   
To be able to use the script ``mmk``, set up the path::
   
   cd mypkg
   export PATH=$PATH:$PWD/mypkg/bin

To check that it is working correctly, you can type::
   
   mmk -help

How to structure the package 
============================ 

We first create a package with its configuration files, using the script ``mmk``. We create a directory ``newpkg`` that we are going to populate with configuration and source files:
::

   mmk -pkg newpkg -with src,axl

This creates a directory ``newpkg`` with the following structure:
::
   
  . 
  └── newpkg
       ├── CMakeLists.txt
       ├── axl
       │   └── CMakeLists.txt
       └── src
           └── CMakeLists.txt

We have a configuration file ``CMakeLists.txt`` for the package ``newpkg`` and subdirectories
``axl``, ``src`` with their own configuration files ``CMakeLists.txt``.
The directory ``axl`` is for the plugins of ``axl``.
The directory ``src`` is for the libraries of the package ``newpkg``.
Of course, the libraries can be used by the plugins of the package as we will see.

Configuration
=============
The configuration is done with ``cmake``. A good practice is to have a build directory, separated from the source directory. We create this build directory ``newpkg-build``
at the same level as ``newpkg``:
::
   
   mkdir newpkg-build

The structure of the directories is: 
::
   
   .
    ├── newpkg
    │   ├── CMakeLists.txt
    │   ├── axl
    │   │   └── CMakeLists.txt
    │   └── src
    │       └── CMakeLists.txt
    └── newpkg-build

We now configure the package in the folder ``newpkg-build`` as follows:
::
   
   cd newpkg-build
   cmake ../newpkg -DAxl_DIR=$PWD/../../build-axl

The option ``-DAxl_DIR=/path/to/build-axl`` specifies where ``axl`` has been built.

.. warning:: The full path to the directory ``build-axl`` should be given in this option of ``cmake``.

We can now check that the compilation builds correctly:
::
   
   make

Nothing is compiled so far, since we have not yet added any source file.


How to create a plugin for Axl 
===============================
We are going now to create a plugin ``newTools``. For that purpose, we first create
a directory with this name in ``newpkg/axl`` :
::
   
   mmk -in ../newpkg/axl -plugin newTools 


The directory ``newTools`` has been added in ``newpkg/axl`` with its configuration file. It contains all the files necessary to build the plugin ``newTools``:
the header file ``newToolsPlugin.h``, the main file ``newToolsPlugin.cpp``, the file ``newToolsPlugin.json`` describing the plugin and the export file ``newToolsExport.h`` (for Windows export mechanism).

The structure of the package is now:
::
   
   newpkg
   ├── CMakeLists.txt
   ├── axl
   │   ├── CMakeLists.txt
   │   └── newTools
   │       ├── CMakeLists.txt
   │       ├── newToolsExport.h
   │       ├── newToolsPlugin.cpp
   │       ├── newToolsPlugin.h
   │       └── newToolsPlugin.json
   └── src
       └── CMakeLists.txt


How to add a new process in the Axl plugin
===========================================

Now we create a new process, called ``myProcess``:
::
   
   mmk -in ../newpkg/axl/newTools -process myProcess

The files ``myProcess[.h|.cpp]`` contain the corresponding process class.
The main file of the plugin ``newToolPlugin.cpp`` contains an ``initialize`` function, which has been updated to register this process.
::
   
   newpkg
   ├── CMakeLists.txt
   ├── axl
   │   ├── CMakeLists.txt
   │   └── newTools
   │       ├── CMakeLists.txt
   │       ├── myProcess.cpp
   │       ├── myProcess.h
   │       ├── newToolsExport.h
   │       ├── newToolsPlugin.cpp
   │       ├── newToolsPlugin.h
   │       └── newToolsPlugin.json
   └── src
       └── CMakeLists.txt

We can now compile these files and generate the plugin ``libmyToolsPlugin``, which is put in the folder ``${AXL_PLUGIN_DIR}``:
::
   
   cmake . 
   make

.. warning:: ``cmake`` has to be run again, to update the list of files to compile, when new files are added.

Executing ``axl``, we can see now in the ``I/tool`` tab, under the tag ``Process``
a function called ``myProcess``. Double-cliking on it and on the button run should produce a red sphere in the view of ``axl``.

The interface of the process is described by the method ``form``. See here for more details.

The process can also be stored in file, such as ``myprocess.axl`` (see mypkg/data)::
  
     <axl>

     <process type="myProcess" status="dynamic" name="Proc1">
     <output name="Output1"  color= "255 255 0"/>
     </process>

     </axl>

It can be evaluated by reading the file ``axl myprocess.axl``. Notice that the color of the output has changed, due to the color attribute in ``<output name="Output1"  color= "255 255 0"/>``.

