#!/usr/bin/perl
use Getopt::Long;
use File::Find::Rule;
use File::Basename;
use File::Path qw(remove_tree);
use Cwd;
use Cwd 'abs_path';

my $SRC = abs_path(dirname($0)."/../")."/";

#----------------------------------------------------------------------
# Create a file from a template file.
# Substitute $x by $r and $X by $R
sub create_from_template {
  my ($template,$file,$x,$r) = @_;

  my $X = uc($x);
  my $R = uc($r);
  my $username = getpwuid( $< );
  my $date = localtime();

  open (IN, $template) || die "could not open $template for read";
  @lines=<IN>;
  close IN;

  open (OUT,">", $file) || die "can not open file ".$file." for write";

  foreach $line (@lines)
    {
      $line =~ s/$x/$r/g;
      $line =~ s/$X/$R/g;
      $line =~ s/my-contact/$username/g;
      $line =~ s/my-date/$date/g;
      $line =~ s/my-email/$email/g;
      $line =~ s/my-website/$website/g;
      $line =~ s/\(C\) my-company/$company/g;
      print OUT $line;
    }
  close (OUT);
}

#----------------------------------------------------------------------
sub skip_between_tag {
  my ($file, $begin, $end) = @_;

  open (IN, $file) || die "could not open ".$file." for read";
  @lines=<IN>;
  close IN;

  open (OUT,">", $file) || die "can not open file ".$file." for write";

  my $p = 1;
  foreach $line (@lines) {
    if ( $line =~ /$begin/ ) {
      $p = 0;
      print OUT $line;
    } elsif ( $line =~ /$end/ ) {
      $p = 1;
    }
    if ( $p ) {
      print OUT $line;
    }
  }
  close (OUT);
}

#----------------------------------------------------------------------
sub replace {
  my ($file,$x,$r) = @_;

  my $X = uc($x);
  my $R = uc($r);

  open (IN, $file) || die "could not open ".$file." for read";
  @lines=<IN>;
  close IN;

  open (OUT,">", $file) || die "can not open file ".$file." for write";

  foreach $line (@lines) {
      $line =~ s/$x/$r/g;
      $line =~ s/$X/$R/g;
      print OUT $line;
  }
  close (OUT);
}

#----------------------------------------------------------------------
sub extract_plugin {

  my @files = File::Find::Rule->in('.');
  foreach my $f (@files) {
    if ($f =~ /Export.h/ ) {
      $plugin = $f;
      $plugin  =~ s/Export.h//g;
    }
  }
  return $plugin;
}

#----------------------------------------------------------------------
sub match_txt {
  my ($x) =@_;
  return $x;
}

#----------------------------------------------------------------------
sub match_registered {
  my ($x) =@_;
  my $str = $x."::registered";
  return $str;
}

#----------------------------------------------------------------------
sub print_registered {
  my ($out, $x) =@_;
  print $out "    if(!".$x."::registered()) { dtkWarn() << \"Unable to register ".$x." type\"; }\n";
}

#----------------------------------------------------------------------
sub print_registered_cpp {
  my ($out, $f) = @_;
  if ($f =~ /.cpp/ && not $f =~ /Plugin.cpp/ ) {
    $f  =~ s/.cpp//g;
    print_registered ($out, $f);
  }
}

#----------------------------------------------------------------------
sub print_registered_data {
  my ($out, $x) =@_;
  print_registered ($out, $x);
  print_registered ($out, $x."Reader");
  print_registered ($out, $x."Writer");
}

#----------------------------------------------------------------------
sub print_registered_process {
  my ($out, $x) =@_;
  print_registered ($out, $x);
  print_registered ($out, $x."Dialog");
}

#----------------------------------------------------------------------
sub print_registered_process_wf {
  my ($out, $x) =@_;
  print_registered ($out, $x);
}

    
#----------------------------------------------------------------------
sub match_include {
  my ($x) =@_;
  my $str = "include \"".$x.".h\"";
  return $str;
}

#----------------------------------------------------------------------
sub print_include {
  my ($out, $x) =@_;
  $x  =~ s/\.cpp//g;
  $x  =~ s/\.h//g;
  $x  =~ s/\.hpp//g;
  print $out "#include \"".$x.".h\"\n";
}

#----------------------------------------------------------------------
sub print_include_data {
  my ($out, $x) =@_;
  print_include($out, $x);
  print_include($out,$x."Reader");
  print_include($out,$x."Writer");
}

#----------------------------------------------------------------------
sub print_include_process {
  my ($out, $x) =@_;
  print_include($out, $x);
  print_include($out,$x."Dialog");
}

sub print_include_process_wf {
  my ($out, $x) =@_;
  print_include($out, $x);
}

#----------------------------------------------------------------------
sub match_types {
  my ($x) =@_;
  my $str = "<< \"".$x;
  return $str;
}

#----------------------------------------------------------------------
sub print_types {
  my ($out, $x) =@_;
  $x  =~ s/.cpp//g;
  $x  =~ s/.h//g;
  $x  =~ s/.hpp//g;
  if (not $x =~ /Plugin/ ) {
    print $out "        << \"".$x."\"\n";
  }
}
#----------------------------------------------------------------------
sub print_cmake_subdir {
  my ($out, $x) =@_;
  if ($x ne '.') {
    print $out "add_subdirectory(".$x.")\n";
  }
}
#----------------------------------------------------------------------
sub print_tab_subdir {
  my ($out, $x) =@_;
  if ($x ne '.') {
    print $out "  ".$x."\n";
  }
}
#----------------------------------------------------------------------
sub add_between_tag {
  my ($file, $x, $match, $printer, $begin, $end) = @_;

  open (IN, "$file") || die "could not open ".$file." for read";
  @lines=<IN>;
  close IN;

  open (OUT,">", $file) || die "can not open file ".$file." for write";

  my $p = 1;
  my $m = 0;
  my $found = 0;
  my $found_begin = 0;
  my $find = $match->($x);
  
  foreach my $line (@lines) {
    if ($line =~ /$begin/ ) {
      $found_begin = 1;
    }
    if ($p && $line =~ /$find/ ) {
      $p = 0;
      $m = 1;
    } elsif ( $line =~ /$end/) {
       if ( $p ) {
	 $printer->(OUT, $x);
	 $found = 1;
       }
       $p = 0;
     }
     print OUT "$line";
   }
  if($found ne 1) {
    if ($m) {
      print "Warning: ".$x." already in ".$begin." of ".$file."\n";
    } else {
      print "Cannot add ".$x."\n";
    }
  }
  close(OUT);
}

#----------------------------------------------------------------------
sub  add_registered_data {
  my ($file, $x) = @_;
  add_between_tag($file,$x,\&match_registered,\&print_registered_data,'<registration>', '<\/registration>');
  add_between_tag($file,$x,\&match_include,\&print_include_data,'<header>', '<\/header>');
  add_between_tag($file,$x,\&match_types,\&print_types,'<types>', '<\/types>'); 
}

#----------------------------------------------------------------------
sub  add_registered_process {
  my ($file, $x) = @_;
  add_between_tag($file,$x,\&match_include,\&print_include_process,'<header>', '<\/header>'); 
  add_between_tag($file,$x,\&match_registered,\&print_registered_process,'<registration>', '<\/registration>'); 
  add_between_tag($file,$x,\&match_types,\&print_types,'<types>', '<\/types>'); 
}

#----------------------------------------------------------------------
sub  add_registered_process_wf {
  my ($file, $x) = @_;
  add_between_tag($file,$x,\&match_include,\&print_include_process_wf,'<header>', '<\/header>'); 
  add_between_tag($file,$x,\&match_registered,\&print_registered_process_wf,'<registration>', '<\/registration>'); 
  add_between_tag($file,$x,\&match_types,\&print_types,'<types>', '<\/types>'); 
}

#----------------------------------------------------------------------
sub  insert_between_tag {
  my ($file, $lister, $begin, $end, $function) = @_;

  open (IN, "$file") || die "could not open ".$file." for read";
  @lines=<IN>;
  close IN;

  open (OUT,">", $file) || die "can not open file ".$file." for write";

  my $files = $lister->();

  my $p = 1;
  foreach my $line (@lines) {
     if ($line =~ /$begin/ ) {
       $p = 0;
       print OUT $line;
       foreach my $f (@$files) {
	 $function->(OUT,$f);
       }
     } elsif ( $line =~ /$end/) {
       $p = 1;
     }
     if( $p ) {
       print OUT $line;
     }
   }
  close (OUT);
}

#----------------------------------------------------------------------
sub txt {
  my ($x) = @_;
  my $r = $x;
  if($x eq "include") {
    $r= "0".$x;
  } elsif ($x eq "src") {
    $r = "1".$x;
  } elsif ($x eq "axl") {
    $r = "2".$x;
  } elsif ($x eq "test") {
    $r = "3".$x;
  }
  return $r;
}
#----------------------------------------------------------------------
sub list_subdir {
  my @list = File::Find::Rule ->directory() ->maxdepth(1)->in('.');
  my @list_sorted = sort { txt($a) cmp txt($b) } @list;
  return \@list_sorted;
}

#----------------------------------------------------------------------
sub list_rec_subdir {
  my @list = File::Find::Rule ->directory() ->in('.');
  return \@list;
}

#----------------------------------------------------------------------
sub list_cpp_files {
  #  my @list = File::Find::Rule->file()->name('*.cpp' )->in('.');
  my @list = glob("*.cpp");
  return \@list;
}

#----------------------------------------------------------------------
sub list_h_files {
  # my @list = File::Find::Rule->file()->name('*.h' )->in('.');
  my @list = glob("*.h");
  return \@list;
}

######################################################################
sub print_cmake_subdir {
  my ($out, $x) =@_;
  if ($x ne '.' && -f $x."/CMakeLists.txt") {
    print $out "add_subdirectory(".$x.")\n";
  }
}
#----------------------------------------------------------------------
sub print_opt_subdir {
  my ($out, $x) =@_;
  if ($x ne '.') {
    if(-f $x."/CMakeLists.txt") {
      print $out "set (SUBDIR \${SUBDIR} ".$x.")\n";
    } else {
    }
  }
}
#----------------------------------------------------------------------
sub print_cmake {
  my ($out, $x) =@_;
    print $out "  ".$x."\n";
}
#----------------------------------------------------------------------
sub create_cmakelists {
  my $template = $SRC."/src/mypkg/CMakeLists.txt";
  my $file = "CMakeLists.txt";
  my ($libname) = basename( getcwd() );
  create_from_template ($template, $file, "mypkg", $libname);
}

#----------------------------------------------------------------------
sub create_plugin {
  my ($x) = @_;

 if (not -d $x) {
   mkdir ($x);
  } else {
    print(">>> Directory $x exists\n");
  }

  $template = $SRC."/axl/myTools/CMakeLists.txt";
  $file =  $x."/CMakeLists.txt";
  create_from_template($template, $file, "myTools", $x);

  $template = $SRC."/axl/myTools/myToolsPlugin.h";
  $file = $x."/".$x."Plugin.h";
  create_from_template($template, $file, "myTools", $x);

  $template = $SRC."/axl/myTools/myToolsPlugin.cpp";
  $file =  $x."/".$x."Plugin.cpp";
  create_from_template($template, $file, "myTools", $x);
  skip_between_tag($file, "<header>", "<\/header>");
  skip_between_tag($file, "<registration>", "<\/registration>");
  skip_between_tag($file, "<types>", "<\/types>");

  $template = $SRC."/axl/myTools/myToolsPlugin.json";
  $file =  $x."/".$x."Plugin.json";
  create_from_template($template, $file, "myTools", $x);

  $template = $SRC."/axl/myTools/myToolsExport.h";
  $file =  $x."/".$x."Export.h";
  create_from_template($template, $file, "myTools", $x);

  update();

}

#----------------------------------------------------------------------
sub update {
  my $plugin = extract_plugin();
  my $file= $plugin."Plugin.cpp";
  if (-f $file) {
    insert_between_tag($file,\&list_cpp_files,'<registration>', '<\/registration>', \&print_registered_cpp);
    insert_between_tag($file,\&list_cpp_files,'<header>', '<\/header>', \&print_include);
    insert_between_tag($file,\&list_cpp_files,'<types>', '<\/types>', \&print_types);
  }
  if (-f "CMakeLists.txt") {
    insert_between_tag ("CMakeLists.txt", \&list_subdir,
			"<subdirectories>", "</subdirectories>",
			\&print_opt_subdir);
  } else {
    my ($dir) = getcwd() ;
    my ($name) =  basename( $dir );
    my $template = $SRC."/".$name."/CMakeLists.txt";
    if (-f $template) {
       create_from_template($template, "CMakeLists.txt", "mypkg", $x);
    } else {
      create_cmakelists;
    }
    if(-f "CMakeLists.txt") {
      insert_between_tag ("CMakeLists.txt", \&list_subdir,
			  "<subdirectories>", "</subdirectories>",
			  \&print_opt_subdir);
    }
  }
}

#----------------------------------------------------------------------
sub create_data {
  my ($x) = @_;
  
  my $plugin = extract_plugin();
  
  my $template = $SRC."/axl/myTools/myData.h";
  my $file = $x.".h";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myData.cpp";
  $file = $x.".cpp";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myDataReader.h";
  $file = $x."Reader.h";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);
  
  $template = $SRC."/axl/myTools/myDataReader.cpp";
  $file = $x."Reader.cpp";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myDataWriter.h";
  $file = $x."Writer.h";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);
  
  $template = $SRC."/axl/myTools/myDataWriter.cpp";
  $file = $x."Writer.cpp";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);

  add_registered_data($plugin."Plugin.cpp", $x);
}
#----------------------------------------------------------------------
sub create_converter {
  my ($x) = @_;
  my $plugin = extract_plugin();

  $template = $SRC."/axl/myTools/myDataConverter.h";
  $file     = $x."Converter.h";
  print("$x $template\n");
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myDataConverter.cpp";
  $file     = $x."Converter.cpp";
  create_from_template($template, $file, "myData", $x);
  replace($file, "myTools", $plugin);

  add_registered_process($plugin."Plugin.cpp", $x."Converter");
}

#----------------------------------------------------------------------
sub create_process {
  my ($x) = @_;
  
  my $plugin = extract_plugin();
  
  $template = $SRC."/axl/myTools/myProcess.h";
  $file = $x.".h";
  create_from_template($template, $file, "myProcess", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myProcess.cpp";
  $file = $x.".cpp";
  create_from_template($template, $file, "myProcess", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myProcessDialog.h";
  $file = $x."Dialog.h";
  create_from_template($template, $file, "myProcess", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myProcessDialog.cpp";
  $file = $x."Dialog.cpp";
  create_from_template($template, $file, "myProcess", $x);
  replace($file, "myTools", $plugin);

  add_registered_process($plugin."Plugin.cpp", $x);
  
}

#----------------------------------------------------------------------
sub create_process_wf {
  my ($x) = @_;
  
  my $plugin = extract_plugin();
  
  $template = $SRC."/axl/myTools/myProcessWithForm.h";
  $file = $x.".h";
  create_from_template($template, $file, "myProcessWithForm", $x);
  replace($file, "myTools", $plugin);

  $template = $SRC."/axl/myTools/myProcessWithForm.cpp";
  $file = $x.".cpp";
  create_from_template($template, $file, "myProcessWithForm", $x);
  replace($file, "myTools", $plugin);

  add_registered_process_wf($plugin."Plugin.cpp", $x);
  
}
#----------------------------------------------------------------------
sub create_pkg {
  my ($x) = shift;

  my ($wd) = getcwd();
  if (not -d $x) {
    mkdir ($x);
  }
  $template = $SRC."/CMakeLists.txt";
  $file = $x."/CMakeLists.txt";
  create_from_template($template, $file, "mypkg", $x);
  foreach my $dir (@_) {
    mkdir ($x."/".$dir);
    if(-f $SRC.$dir."/CMakeLists.txt") {
      create_from_template($SRC.$dir."/CMakeLists.txt",$x."/".$dir."/CMakeLists.txt", "mypkg", $x);
      skip_between_tag($x."/".$dir."/CMakeLists.txt", "<subdirectories>", "<\/subdirectories>");
    }
  }
  if(-f $x."/CMakeLists.txt") {
    chdir($x);
    insert_between_tag ("CMakeLists.txt", \&list_subdir,
			"<subdirectories>", "</subdirectories>",
			\&print_opt_subdir);
    chdir($wd);
  }
}

#----------------------------------------------------------------------
sub init_pkg {
  my ($name) = shift;
  my ($wd) = getcwd();

  $template = $SRC."/CMakeLists.txt";
  $file = $wd."/CMakeLists.txt";
  create_from_template($template, $file, "mypkg", $name);
  foreach my $dir (@_) {
    mkdir ($wd."/".$dir);
    if(-f $SRC.$dir."/CMakeLists.txt") {
      create_from_template($SRC.$dir."/CMakeLists.txt",$wd."/".$dir."/CMakeLists.txt", "mypkg", $name);
      skip_between_tag($wd."/".$dir."/CMakeLists.txt", "<subdirectories>", "<\/subdirectories>");
    }
  }
  if(-f $wd."/CMakeLists.txt") {
    insert_between_tag ("CMakeLists.txt", \&list_subdir,
			"<subdirectories>", "</subdirectories>",
			\&print_opt_subdir);
    chdir($wd);
  }
}

#----------------------------------------------------------------------
sub create_dir {
  my ($dir) = @_;

  if(not -d $dir) { mkdir $dir; }

  if(-f $SRC."/".$dir."/CMakeLists.txt") {
    create_from_template($SRC.$dir."/CMakeLists.txt", $dir."/CMakeLists.txt", "mypkg", $x);
    skip_between_tag($dir."/CMakeLists.txt", "<subdirectories>", "<\/subdirectories>");
    
  } elsif ($cmake ne 'not_defined') {
    my $template = $SRC."/src/CMakeLists.txt";
    my ($libname) = basename( $dir );
    create_from_template ($template, $dir."/CMakeLists.txt", "mypkg", $libname);
  }
  
  if(-f "CMakeLists.txt") {
    insert_between_tag ("CMakeLists.txt", \&list_subdir,
			"<subdirectories>", "</subdirectories>",
			\&print_opt_subdir);
  }
}

#----------------------------------------------------------------------
sub remove_dir {
  my ($dir) = @_;

  if(-d $dir) {
    print("--- removing $dir\n");
    remove_tree($dir);
  }

  if(-f "CMakeLists.txt") {
    insert_between_tag ("CMakeLists.txt", \&list_subdir,
			"<subdirectories>", "</subdirectories>",
			\&print_opt_subdir);
  }
}
######################################################################
$USAGE = q(
NAME
     mmk -- configuration generator

SYNOPSIS
     mmk <options>

DESCRIPTION
     Generate configuration files and template file for packages that provide 
     plugins for Axel.

OPTIONS
     -in<DIR>           Generate files or subdirectories in the directory 
                         <DIR>. If <DIR> does not exists, it creates it. 

     -dir <DIR>          Create the directory <DIR> and update the CMakeLists.txt.

     -rm <DIR>           Remove the directory <DIR> and update the CMakeLists.txt.

     -pkg <NAME>         Create the directory <NAME> and its CMakeLists.txt.

     -with <D1>,<D2>...  If -with is used with -pkg, it creates the sub-
                         directories <D1> <D2> ... and the associate configu-
                         ration files. The names of the subdirectories should 
                         be separated by "," with no space.

     -plugin <NAME>      Create the directory <NAME> and inside the template 
                         plugin <NAME> and its associate files.

     -process <NAME>     Create the template process <NAME> and its associate 
                         files. 

     -wd                 If the option -wd is used with -process, the template
                         process is generated with a dialog class template. 

     -data <NAME>        Create the template data <NAME> and its associate
                         files.

     -converter <NAME>   Create the template converter for data <NAME> and 
                         its associate files.

     -update|-up         Update the configutation file CMakeLists.txt. 

     -help|-h            Display this message.

EXAMPLE
     mmk -pkg newpkg -with src,axl
     mmk -in newpkg/axl -dir myTools
     mmk -in newpkg/axl/myTools -plugin newTools -process myFct  
     mmk -in newpkg/axl/myTools -process newFct -wd 

);

######################################################################
# setup defaults
my $in        = 'not_defined';
my $init      = 'not_defined';
my $pkg       = 'not_defined';
my $plugin    = 'not_defined';
my $data      = 'not_defined';
my $converter = 'not_defined';
my $process   = 'not_defined';
my $directory = 'not_defined';
my $wd        = 0;
my $update    = 0;
my $cmake     = 0;
my $help      = 0;
my $man       = 0;


GetOptions(
	   'in=s'        => \$in,
	   'dir|add=s'   => \$directory,
   	   'rm=s'        => \$rmdirectory,
	   'cmake'       => \$cmake,
	   'pkg=s'       => \$pkg,
	   'with=s'      => \@with,
	   'plugin=s'    => \$plugin,
	   'data=s'      => \$data,
	   'converter=s' => \$converter,
	   'process=s'   => \$process,
	   'wd'          => \$wd,	   
	   'update|up'   => \$update,
	   'help|?|h'    => \$help,
	   'init=s'      => \$init,
	   'man'         => \$man)
  or die $USAGE;

if ($help) {  die $USAGE; }

if ( $in ne 'not_defined' )  {
  if( not -d $in ) {
    system("mkdir -p $in");
  }
  chdir($in);
}

@with = split(/,/,join(',',@with));

if ($init ne 'not_defined')         { init_pkg($init, @with); }
if ($pkg ne 'not_defined')          { create_pkg($pkg, @with); }
if ($directory ne 'not_defined')    { create_dir($directory); }
if ($rmdirectory ne 'not_defined')  { remove_dir($rmdirectory); }

if( $plugin ne 'not_defined' )    { create_plugin($plugin); }
if( $data ne 'not_defined' )      { create_data($data); }
if( $converter ne 'not_defined' ) { create_converter($converter); }

if( $process ne 'not_defined' )   {
  if ($wd)
    { create_process($process); }
  else
    { create_process_wf($process); }
}

if ( $update ) { update(); }
#if ( $cmake )  { create_cmakelists(); }


